<?php

/**
 * Created by PhpStorm.
 * User: tppli
 * Date: 13.01.2018
 * Time: 14:13
 */

require_once 'libs/Controller.php';

class ListVehicles extends Controller {
    public function indexAction() {
        $this->getListClientsVehicles();
    }

    public function getListClientsVehicles() {
        $naglowki = array(
            "Content-Type: application/json"
        );

        $curl = curl_init();
        if (!$curl) {
            exit(1);
        }
        $opcjeCurl = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $naglowki,
            CURLOPT_URL => "http://i5b1n1-cepik.herokuapp.com/vehicle/list",
        );
        if (!curl_setopt_array($curl, $opcjeCurl)) {
            exit(2);
        }
        $wynik = curl_exec($curl);

        if ($wynik === false) {
            exit(3);
        }

        $wyniki = json_decode($wynik, true);


        if(curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200) {
            $this->view->setRender('listVehicles/index', ["listVehicles" => $wyniki]);
        } else {
            $this->view->setRender('listVehicles/index');
        }
    }

    public function detailsAction() {
        $id = $_GET['id'];

        $naglowki = array(
            "Content-Type: application/json"
        );

        $curl = curl_init();
        if (!$curl) {
            exit(1);
        }
        $opcjeCurl = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $naglowki,
            CURLOPT_URL => "http://i5b1n1-cepik.herokuapp.com/vehicle/find/$id",
        );
        if (!curl_setopt_array($curl, $opcjeCurl)) {
            exit(2);
        }
        $wynik = curl_exec($curl);

        if ($wynik === false) {
            exit(3);
        }

        $wyniki = json_decode($wynik, true);

        $annotationList = $this->getAnnotationList($id);

        if(curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200) {
            $this->view->setRender('listVehicles/details', ["detailsVehicle" => $wyniki, "annotationList" => $annotationList]);
        } else {
            $this->view->setRender('listVehicles/details');
        }
    }

    public function updateVehicleAction() {
        $_POST['id'] = intval($_POST['id']);

        $naglowki = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen(json_encode($_POST))
        );

        $curl = curl_init();
        if (!$curl) {
            exit(1);
        }

        $opcjeCurl = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => json_encode($_POST),
            CURLOPT_CUSTOMREQUEST => "POST",
            //CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => $naglowki,
            CURLOPT_URL => "http://i5b1n1-cepik.herokuapp.com/vehicle/edit",
            CURLOPT_VERBOSE => true,
            CURLOPT_HEADER => true,
            CURLINFO_HEADER_OUT => true
        );
        if (!curl_setopt_array($curl, $opcjeCurl)) {
            exit(2);
        }
        $wynik = curl_exec($curl);

        if ($wynik === false) {
            exit(3);
        }

        if(curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200) {
            header('Refresh: 4; url=/ListVehicles/index');
            echo "<body style='background-color: #E0FFFF'><center><h1 style='margin-top: 250px;'>Edycja danych pojazdu przebiegła pomyślnie!</h1></center></body>";
        } else {
            header('Refresh: 4; url=/ListVehicles/index');
            echo "<body style='background-color: #E0FFFF'><center><h1 style='margin-top: 250px;'>System napotkał problem, spróbuj ponownie!</h1></center></body>";
        }
    }

    public function getAnnotationList($id) {
        $naglowki = array(
            "Content-Type: application/json"
        );

        $curl = curl_init();
        if (!$curl) {
            exit(1);
        }
        $opcjeCurl = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $naglowki,
            CURLOPT_URL => "http://i5b1n1-cepik.herokuapp.com/annotation/list/id/$id",
        );
        if (!curl_setopt_array($curl, $opcjeCurl)) {
            exit(2);
        }
        $wynik = curl_exec($curl);

        if ($wynik === false) {
            exit(3);
        }

        $wyniki = json_decode($wynik, true);

        return $wyniki;

    }

    private function varDumper($data) {
        echo '<pre>' . var_export($data, true) . '</pre>';
    }

}