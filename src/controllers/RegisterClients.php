<?php

/**
 * Created by PhpStorm.
 * User: tppli
 * Date: 17.12.2017
 * Time: 14:35
 */

require_once 'libs/Controller.php';

class RegisterClients extends Controller {

    public function indexAction() {
        $this->view->render('registerClients/index');
    }

    public function addUserAction() {
        unset($_POST['save']);
        //$this->checkValidateForm();

        $_POST['licenceCategories'] = array("A", "C", "D");
        
        $naglowki = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen(json_encode($_POST))
        );

        $curl = curl_init();
        if (!$curl) {
            exit(1);
        }

        $opcjeCurl = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => json_encode($_POST),
            CURLOPT_CUSTOMREQUEST => "POST",
            //CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => $naglowki,
            CURLOPT_URL => "http://i5b1n1-cepik.herokuapp.com/person/create",
            CURLOPT_VERBOSE => true,
            CURLOPT_HEADER => true,
            CURLINFO_HEADER_OUT => true
        );
        if (!curl_setopt_array($curl, $opcjeCurl)) {
            exit(2);
        }
        $wynik = curl_exec($curl);

        if ($wynik === false) {
            exit(3);
        }

        if(curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200) {
            header('Refresh: 4; url=/ListClients/index');
            echo "<body style='background-color: #E0FFFF'><center><h1 style='margin-top: 250px;'>Użytkownik został dodany do systemu pomyślnie!</h1></center></body>";
        } else {
            header('Refresh: 4; url=/RegisterClients/index');
            echo "<body style='background-color: #E0FFFF'><center><h1 style='margin-top: 250px;'>System napotkał problem, spróbuj ponownie!</h1></center></body>";
        }

    }

    private function checkValidateForm() {
        $this->varDumper($_POST);
        $exp = '/^[0-9]{2}-?[0-9]{3}$/';
        $this->checkPostalCode($exp, $_POST['postalCode']);
        if(isset($_POST['birthdayDate'])) {
            $this->checkDateFormat($_POST['birthdayDate']);
        }
    }

    private function checkDateFormat($date)
    {
        if($date == '') {
            echo 'pusta data';
        } elseif (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date, $parts)){
            if(checkdate($parts[2],$parts[3],$parts[1])) {
                echo 'OK!';
            } else {
                echo 'niepoprawna data!';
            }
        } else {
            echo 'niepoprawna data!';
        }
    }

    private function checkPostalCode($expression, $value) {
        $wynik = (bool)preg_match($expression, $value);

        if($wynik == false) {
            header('location: /RegisterClients/index');
        }
    }

    private function varDumper($data) {
        echo '<pre>' . var_export($data, true) . '</pre>';
    }

}