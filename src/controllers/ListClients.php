<?php

/**
 * Created by PhpStorm.
 * User: tppli
 * Date: 13.01.2018
 * Time: 14:13
 */

require_once 'libs/Controller.php';

class ListClients extends Controller {
    public function indexAction() {
        $this->getListClients();
    }

    public function getListClients() {
        $naglowki = array(
            "Content-Type: application/json"
        );

        $curl = curl_init();
        if (!$curl) {
            exit(1);
        }
        $opcjeCurl = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $naglowki,
            CURLOPT_URL => "http://i5b1n1-cepik.herokuapp.com/person/list",
        );
        if (!curl_setopt_array($curl, $opcjeCurl)) {
            exit(2);
        }
        $wynik = curl_exec($curl);

        if ($wynik === false) {
            exit(3);
        }

        $wyniki = json_decode($wynik, true);

        if(curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200) {
            $this->view->setRender('listClients/index', ["listClients" => $wyniki]);
        } else {
            $this->view->setRender('listClients/index');
        }
    }

    private function varDumper($data) {
        echo '<pre>' . var_export($data, true) . '</pre>';
    }

}