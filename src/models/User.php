<?php

/**
 * Created by PhpStorm.
 * User: tppli
 * Date: 02.12.2017
 * Time: 16:23
 */
class User
{
    private $name;
    private $age;

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getAge() {
        return $this->age;
    }

    public function setAge($age) {
        $this->age = $age;
    }
}