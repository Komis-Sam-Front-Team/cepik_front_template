/**
 * Created by tppli on 17.12.2017.
 */

function postcode_validate(postcode)
{
    var regPostcode = /^[0-9]{2}-?[0-9]{3}$/;

    if(regPostcode.test(postcode) == false)
    {
        document.getElementById("status").innerHTML = "Kod pocztowy nie jest poprawny.";
    }
    else
    {
        document.getElementById("status").innerHTML = "Poprawny kod pocztowy!";
    }
}

