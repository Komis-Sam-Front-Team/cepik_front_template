<?php

/**
 * Created by PhpStorm.
 * User: tppli
 * Date: 23.12.2017
 * Time: 19:03
 */

require_once 'libs/Controller.php';

class RegisterVehicles extends Controller {

    public function indexAction() {
        $this->view->render('registerVehicles/index');
    }

    public function addVehicleAction() {
        unset($_POST['save']);

        $naglowki = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen(json_encode($_POST))
        );

        $curl = curl_init();
        if (!$curl) {
            exit(1);
        }

        $opcjeCurl = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => json_encode($_POST),
            CURLOPT_CUSTOMREQUEST => "POST",
            //CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => $naglowki,
            CURLOPT_URL => "http://i5b1n1-cepik.herokuapp.com/vehicle/register",
            CURLOPT_VERBOSE => true,
            CURLOPT_HEADER => true,
            CURLINFO_HEADER_OUT => true
        );
        if (!curl_setopt_array($curl, $opcjeCurl)) {
            exit(2);
        }
        $wynik = curl_exec($curl);

        if ($wynik === false) {
            exit(3);
        }

        if(curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200) {
            header('Refresh: 4; url=/ListVehicles/index');
            echo "<body style='background-color: #E0FFFF'><center><h1 style='margin-top: 250px;'>Pojazd został zarejestrowany pomyślnie!</h1></center></body>";
        } else {
            header('Refresh: 4; url=/RegisterVehicles/index');
            echo "<body style='background-color: #E0FFFF'><center><h1 style='margin-top: 250px;'>System napotkał problem, spróbuj ponownie!</h1></center></body>";
        }

    }
}